var express = require('express');
var fs = require('fs');
var router = express.Router();
var multer = require('multer');
var spawn  = require('child_process').spawnSync;
var upload = multer({dest:'uploads/'}).single('recFile');
var $ = jQuery = require('jquery');
require('./jquery.csv.js');
var hashMapping = {};


/* GET home page. */
router.get('/', function(req, res, next) {
   res.render('index', { title: 'Express',graphData:[]});
});

// Setup upload.
router.post('/upload', function(req, res, next) {
   upload(req,res, function(err){
      if (err){
         res.send(err);
         console.log(err);
      } else {
         var outputString = JSON.stringify({
            filename: req.file.originalname,
            size: req.file.size,
            hash: req.file.filename
         });
         var location = __dirname+"/../"+req.file.destination+req.file.filename
         var fileHash;
         try{
            var apiCall = spawn('curl',['-X','POST','-F','recFile=@'+location,'optimum.herokuapp.com/events/upload']);
            console.log(apiCall.stdout.toString());
            var uploaded = JSON.parse(apiCall.stdout.toString());
            fileHash = uploaded.hash;
            hashMapping[fileHash] = req.file.filename;
            res.send(fileHash);
         } catch (e){
            console.log(e);
         }
      }
   });
});

router.post('/new-study', function(req, res, next) {
   data = req.body;
   var file1Hash = data["file1Hash"];
   var file2Hash = data["file2Hash"];
   var upperWindow = data["upperWindow"];
   var lowerWindow = data["lowerWindow"];
   var stockID = [];
   var otherParameters = {};
   var currVar;
   var checkFlag;
   var currRIC;

   for (key in data){
      if (checkFlag === 2){
         otherParameters[currVar].lower = data[key];
         checkFlag = 0;
      } 
      if (checkFlag === 1){
         otherParameters[currVar].upper = data[key];
         checkFlag = 2;
      } 
      if (key.match(/^var[0-9]*_name$/)){
         currVar = data[key];
         otherParameters[currVar] = {
            "upper": 0,
            "lower": 0
         }
         checkFlag = 1;
      } 
      if (key.match(/^RIC_[0-9]+$/)) {
         stockID.push(data[key]);
      };
   }
   responseJSON = {
      "file1hash": file1Hash,
      "file2hash": file2Hash,
      "stockID" : stockID,
         "window":{
            "upper": upperWindow,
            "lower": lowerWindow
         },
      "otherParameters": otherParameters
   }
   //var sendJSON = JSON.stringify(responseJSON).toString();
   var apiCall = spawn('curl',['-X','POST','-H','content-Type: application/json','optimum.herokuapp.com/events/calculate', '-d', JSON.stringify(responseJSON)]);
   try {
      var output = JSON.parse(apiCall.stdout.toString());
      var labelList = '["'
      var dataList = []
      console.log(output);
      console.log(output.Error);
      var labels = false;
      if (output.Error == true){
         console.log("An Error Occured");
         console.log(output.Results);
         res.send(output.Results);
      } else {
         for (result in output.Results){
            console.log(result);
            if (labels == false){
               var comFlag = false;
               labels = true;   
               for (label in output.Results[result]["Cumulative Returns"]){
                  if (comFlag == true){
                     labelList = labelList + ',"';
                  } else {
                     comFlag = true;
                  }
                  console.log(label);
                  labelList=labelList+label+'"';
               }
               labelList = labelList + ']'
            }
            var newData = [];
            for (label in output.Results[result]["Cumulative Returns"]){
               console.log(output.Results[result]["Cumulative Returns"][label]);
               newData.push(output.Results[result]["Cumulative Returns"][label]);
            }
            dataList.push(newData);
         }
         res.send(output.Results);
      }
   } catch (e) {
      graphScript = 
         console.log("An Error Occured");
         console.log("No response from API server");
      res.send({"errorMessage": "No response from API server"});
   }
});

router.post('/set-new-study', function(req, res, next) {
   data = req.body;
   var dataSet = data["recFile"]
   var upperWindow = data["upperWindow"];
   var lowerWindow = data["lowerWindow"];
   var stockID = [];
   var otherParameters = {};
   var currVar;
   var checkFlag;
   var currRIC;

   for (key in data){
      if (checkFlag === 2){
         otherParameters[currVar].lower = data[key];
         checkFlag = 0;
      } 
      if (checkFlag === 1){
         otherParameters[currVar].upper = data[key];
         checkFlag = 2;
      } 
      if (key.match(/^var[0-9]*_name$/)){
         currVar = data[key];
         otherParameters[currVar] = {
            "upper": 0,
            "lower": 0
         }
         checkFlag = 1;
      } 
      if (key.match(/^RIC_[0-9]+$/)) {
         stockID.push(data[key]);
      };
   }
   responseJSON = {
      "dataSet": dataSet,
      "stockID" : stockID,
         "window":{
            "upper": upperWindow,
            "lower": lowerWindow
         },
      "otherParameters": otherParameters
   }
   //var sendJSON = JSON.stringify(responseJSON).toString();
   var apiCall = spawn('curl',['-X','POST','-H','content-Type: application/json','optimum.herokuapp.com/events/retrieve', '-d', JSON.stringify(responseJSON)]);
   try {
      var output = JSON.parse(apiCall.stdout.toString());
      var labelList = '["'
      var dataList = []
      console.log(output);
      console.log(output.Error);
      var labels = false;
      if (output.Error == true){
         console.log("An Error Occured");
         console.log(output.Results);
         res.send(output.Results);
      } else {
         for (result in output.Results){
            console.log(result);
            if (labels == false){
               var comFlag = false;
               labels = true;   
               for (label in output.Results[result]["Cumulative Returns"]){
                  if (comFlag == true){
                     labelList = labelList + ',"';
                  } else {
                     comFlag = true;
                  }
                  console.log(label);
                  labelList=labelList+label+'"';
               }
               labelList = labelList + ']'
            }
            var newData = [];
            for (label in output.Results[result]["Cumulative Returns"]){
               console.log(output.Results[result]["Cumulative Returns"][label]);
               newData.push(output.Results[result]["Cumulative Returns"][label]);
            }
            dataList.push(newData);
         }
         res.send(output.Results);
      }
   } catch (e) {
      graphScript = 
         console.log("An Error Occured");
         console.log("No response from API server");
      res.send({"errorMessage": "No response from API server"});
   }
});

router.get('/rics', function(req, res, next) {
   data = req.query;
   var fileLoc = "uploads/" + hashMapping[data.file];
   console.log(fileLoc);
   fs.readFile(fileLoc, 'UTF-8', function (err, file) {
      console.log(file);
      $.csv.toObjects(file, {}, function (error, data) {
         if (error) {
            var error = restUtils.errorCode(6,hash1);
            res.send(error);
         } else {
            res.send(getRICS(data));
         }
      });
   });
});

router.get('/params', function(req, res, next) {
   data = req.query;
   var fileLoc = "uploads/" + hashMapping[data.file];
   console.log(fileLoc);
   fs.readFile(fileLoc, 'UTF-8', function (err, file) {
      console.log(file);
      $.csv.toObjects(file, {}, function (error, data) {
         if (error) {
            var error = restUtils.errorCode(6,hash1);
            res.send(error);
         } else {
            res.send(getParams(data));
         }
      });
   });
});

var getRICS = function(data) {
   var rics = [];
   for (var key in data){
      if (! data.hasOwnProperty(key)) continue;
      if (rics.indexOf(data[key]["#RIC"]) == -1){
         rics.push(data[key]["#RIC"]);
      }
   }
   return rics;
}


var getParams = function(data) {
   var params = [];
   if (data.hasOwnProperty(0)){
      for (var key in data[0]) {
         params.push(key);
      }
   }
   return params;
}

module.exports = router;
