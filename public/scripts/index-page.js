// Define some frequently used elements on page
var addNewRICButton = $('#add-new-ric');
var addNewParamButton = $('#add-new-param');
var form = $('#calculate-with-parameters-form');
var setEvents = ['Cash Rate', 'China Growth Rate Change', 'CEO Health issue', 'Earning anouncements', 'Macroevent', 'Government Policy changes'];
var setRICS = {
    "MetalCompanies": ['MLX.AX', 'PLS.AX', 'AQA.AX', 'BRL.AX'],
    "HealthCompanies": ['MYX.AX', 'API.AX', 'FPH.AX', 'REG.AX'],
    "FinanceCompanies": ['ABP.AX', 'ALZ.AX', 'AMP.AX', 'ANZ.AX', 'ASX.AX', 'LLC.AX'],
    "EnergyCompanies": ['BPT.AX', 'CTX.AX', 'LNG.AX', 'CTX.AX', 'ORG.AX'],
    "ConsumerCompanies": ['GUD.AX', 'BRG.AX']
}

// Keep track of how many RICs and parameters have been added.
var param_count = 0;
var RIC_count = 0;
var ricDropdown = [];
var paramDropdown = [];

/**
 * Adds a new parameter and it's corresponding input fields including:
 * - Parameter name
 * - Upper window
 * - Lower window
 * and helpful surrounding elements like a heading and a delete button.
 * Called from 'Add another parameter' button's on click event listener.
 */
 function addNewParam() {
    // Update the amount of new parameters so far for use in JSON parsing
    // of the form later.
    param_count++;

    // Create an input heading 'Variable Name' which will appear above the
    // input text field for the var name.
    var paramNameHeading = $('<p></p>', {
        text: 'Parameter name'
    })
    paramNameHeading.insertBefore(addNewParamButton);

    // Create an input variable name text field.
    var paramSelectField = $('<select>', {
        name: "var" + param_count + "_name",
        class: "form-control param-input",
        form: "calculate-with-parameters-form",
    })
    paramSelectField.insertBefore(addNewParamButton);

    for (var key in paramDropdown){
        if (!paramDropdown.hasOwnProperty(key)) continue;
        paramSelectField.append($("<option>").attr('value',paramDropdown[key].value).text(paramDropdown[key].text));
    };
    // Create a delete button for the new parameter which will link to a delete function,
    // which deletes all corresponding elements with this new parameter from the page.
    var deleteButton = $('<button></button', {
        text: "x",
        class: "btn btn-danger btn-sm param-delete",
        id: "delete_new_param" + param_count,
        click: function () {
            deleteNewParam(paramNameHeading, paramSelectField,
                       upperWindowHeading, upperWindowInputField,
                       lowerWindowHeading, lowerWindowInputField, deleteButton);
        }
    })

    var windowRow = $('<div></div>', {
        class: 'row'
    });

    windowRow.insertBefore(addNewParamButton);
    deleteButton.insertBefore(windowRow);
    var divColumn = $('<div></div>', {
        class: 'col-sm-6'
    });
    // Insert delete button before the 'Upper window' heading.
    windowRow.append(divColumn);

    // Create an input heading 'Upper window' which will appear above the
    // input upper window text field for the var name.
    var upperWindowHeading = $('<p></p>', {
        text: 'Upper Window',
        class: "param-window"
    })
    divColumn.append(upperWindowHeading);

    // Create an upper window text field.
    var upperWindowInputField = $('<input/>', {
        name: "upper_window_var" + param_count,
        placeholder: "e.g. 3",
        class: "param-window form-control",
        type: 'text'
    });
    divColumn.append(upperWindowInputField);


    var divColumn2 = $('<div></div>', {
        class: 'col-sm-6'
    })
    // Insert delete button before the 'Upper window' heading.
    windowRow.append(divColumn2);

    // Create an input heading 'Lower window' which will appear above the
    // input upper window text field for the var name.
    var lowerWindowHeading = $('<p></p>', {
        text: 'Lower Window',
        class: "param-window"
    })
    divColumn2.append(lowerWindowHeading);

    // Create a lower window text field.
    var lowerWindowInputField = $('<input/>', {
        name: "lower_window_var" + param_count,
        placeholder: "e.g. 1",
        class: "param-window form-control",
        type: 'text'
    });
    divColumn2.append(lowerWindowInputField);
};

/**
 * Adds a new RIC input element with other useful surrounding elements like
 * a heading and a delete button.
 * Called from 'Add RIC' button's on click event listener.
 */
function specifyNewRIC() {
    // Update amount of specified RIC's so far for use in JSON parsing of the
    // form later.
    RIC_count++;

    // Create the input heading "RIC" which will appear above the input text field.
    var inputHeading = $('<p></p>', {
        text: 'RIC'
    })
    inputHeading.insertBefore(addNewRICButton);

    // Create the text input
    var selectField = $('<select>', {
        name: "RIC_" + RIC_count,
        class: "form-control ric-input",
        form: "calculate-with-parameters-form",
        placeholder: "RIC Stock ID"
    })
    selectField.insertBefore(addNewRICButton);

    for (var key in ricDropdown) {
        if (!ricDropdown.hasOwnProperty(key)) continue;
        selectField.append($("<option>").attr('value',ricDropdown[key].value).text(ricDropdown[key].text));
    };

    // Create the button to delete the "RIC" attribute
    var deleteButton = $('<button></button>', {
        text: "x",
        class: "btn btn-danger btn-sm ric-delete",
        id: "delete_RIC_" + RIC_count,
        click: function () {
            deleteRIC(inputHeading, selectField, deleteButton);
        }
    })
    deleteButton.insertBefore(addNewRICButton);
}

/**
 * HELPER delete function for deleting RICs.
 */
function deleteRIC(inputHeading, selectField, deleteButton) {
    inputHeading.remove();
    selectField.remove();
    deleteButton.remove();
}

/**
 * HELPER delete function for deleting new params.
 */
function deleteNewParam (paramNameHeading, paramSelectField, upperWindowHeading, upperWindowInputField, lowerWindowHeading, lowerWindowInputField, deleteButton) {
    paramNameHeading.remove();
    paramSelectField.remove();
    upperWindowHeading.remove();
    upperWindowInputField.remove();
    lowerWindowHeading.remove();
    lowerWindowInputField.remove();
    deleteButton.remove();
}

function displayError(output) {
    console.log(output['errorMessage']);
    $('#graphInfo').text(output['errorMessage']);
}

function displayGraph(output) {
    var chartNum = 1;
    var panel = $('#graph-panel');
    panel.empty();
    var chart = $('<canvas></canvas>', {
            id: "complete-chart",
            width: "80vw"
        });
    chart.appendTo(panel);
    var allData = [];
    var sortedKeys = [];
    for(var key in output){
        if (!output.hasOwnProperty(key)) continue;
        var eventOut = output[key];
        var title = eventOut['RIC'] + " | " + eventOut['Event Date'];

        var sortedKeys = [];
        for (var key in eventOut['Cumulative Returns']){
            if (!eventOut['Cumulative Returns'].hasOwnProperty(key)) continue;
            if (sortedKeys.indexOf(parseInt(key)) == -1) {
                sortedKeys.push(parseInt(key));
            }
        }
        sortedKeys = sortedKeys.sort(function(a, b){return a-b});

        var data = [];
        for (var key in sortedKeys){
            if (!sortedKeys.hasOwnProperty(key)) continue;
            data.push(eventOut['Cumulative Returns'][sortedKeys[key].toString()]);
        }

        var redColours = ["#ff0022","#ff0000", "#e50000", "#cc0000","#b20000",
                          "#990000", "#7f0000", "#660000", "#ff0000",
                          "#ff1919", "#ff3232"];
        if (eventOut['RIC'] === 'Average') {
          allData.push({"label": title, "fill": false, "borderColor": "#ff0022", "data": data});
        } else {
          var color = getRandomColor();
          while (redColours.indexOf(color) != -1) {
            color = getRandomColor();
          }
          allData.push({"label": title, "fill": false, "borderColor": color, "data": data});
        }
    }
    console.log(allData);
    if (output.length === 0) {
        $('#graphInfo').text("No data found");
    } else {
        var myChart = new Chart(chart, {
            type: 'line',
            data: {
                labels: sortedKeys,
                datasets: allData
            },
            options: {
                yAxes: [{
                    type: "linear"
                }],
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display:true,
                            labelString: 'Cumalative Return (%)'
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Relative Date'
                        }
                    }]
                }
            }
        });
        $('#graphInfo').text("Study ran successfully.");
    }
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function generateRICDropdown(rics){
    ricDropdown = [];
    for (var key in rics){
        if(!rics.hasOwnProperty(key)) continue;
        ricDropdown.push({val: rics[key], text: rics[key]});
    }
    $('#add-new-ric').show();
}

function refreshDropdowns() {
    // Generates the RICs and add new params dropdown when the user inputs the
    // file hash manually instead of submitting via the form.
    file2Hash = $('#file2Hash').val();
    if (file2Hash) {
        $.ajax({
            url:'/rics',
            type:'GET',
            data: {"file": file2Hash},
            success: function (rics){
                generateRICDropdown(rics);
            }
        });
         $.ajax({
            url:'/params',
            type:'GET',
            data: {"file": file2Hash},
            success: function (params){
                console.log(params);
                generateParamDropdown(params);
            }
         })
    }
    $('#refresh-dropdowns-button').hide();
}

function generateParamDropdown(params){
    paramDropdown = [];
    for (var key in params){
        if(!params.hasOwnProperty(key)) continue;
        if(params[key] == "#RIC" || params[key] == "Event Date") continue;
        paramDropdown.push({val: params[key], text: params[key]});
    }
    $('#add-new-param').show();
    $('#extra-params-heading').show();
}

function setAddNewParam() {
    // Update the amount of new parameters so far for use in JSON parsing
    // of the form later.
    param_count++;

    // Create an input heading 'Variable Name' which will appear above the
    // input text field for the var name.
    var paramNameHeading = $('<p></p>', {
        text: 'Parameter name'
    })
    paramNameHeading.insertBefore($('#set-add-new-param'));

    // Create an input variable name text field.
    var paramSelectField = $('<select>', {
        name: "var" + param_count + "_name",
        class: "form-control param-input",
        form: "set-study-form",
    })
    paramSelectField.insertBefore($('#set-add-new-param'));

    for (var key in paramDropdown){
        if (!paramDropdown.hasOwnProperty(key)) continue;
        paramSelectField.append($("<option>").attr('value',paramDropdown[key]).text(paramDropdown[key]));
    };
    // Create a delete button for the new parameter which will link to a delete function,
    // which deletes all corresponding elements with this new parameter from the page.
    var deleteButton = $('<button></button', {
        text: "x",
        class: "btn btn-danger btn-sm param-delete",
        id: "delete_new_param" + param_count,
        click: function () {
            deleteNewParam(paramNameHeading, paramSelectField,
                       upperWindowHeading, upperWindowInputField,
                       lowerWindowHeading, lowerWindowInputField, deleteButton);
        }
    })

    var windowRow = $('<div></div>', {
        class: 'row'
    });

    windowRow.insertBefore($('#set-add-new-param'));
    deleteButton.insertBefore(windowRow);
    var divColumn = $('<div></div>', {
        class: 'col-sm-6'
    });
    // Insert delete button before the 'Upper window' heading.
    windowRow.append(divColumn);

    // Create an input heading 'Upper window' which will appear above the
    // input upper window text field for the var name.
    var upperWindowHeading = $('<p></p>', {
        text: 'Upper Window',
        class: "param-window"
    })
    divColumn.append(upperWindowHeading);

    // Create an upper window text field.
    var upperWindowInputField = $('<input/>', {
        name: "upper_window_var" + param_count,
        placeholder: "e.g. 3",
        class: "param-window form-control",
        type: 'text'
    });
    divColumn.append(upperWindowInputField);


    var divColumn2 = $('<div></div>', {
        class: 'col-sm-6'
    })
    // Insert delete button before the 'Upper window' heading.
    windowRow.append(divColumn2);

    // Create an input heading 'Lower window' which will appear above the
    // input upper window text field for the var name.
    var lowerWindowHeading = $('<p></p>', {
        text: 'Lower Window',
        class: "param-window"
    })
    divColumn2.append(lowerWindowHeading);

    // Create a lower window text field.
    var lowerWindowInputField = $('<input/>', {
        name: "lower_window_var" + param_count,
        placeholder: "e.g. 1",
        class: "param-window form-control",
        type: 'text'
    });
    divColumn2.append(lowerWindowInputField);
};

/**
 * Adds a new RIC input element with other useful surrounding elements like
 * a heading and a delete button.
 * Called from 'Add RIC' button's on click event listener.
 */
function setSpecifyNewRIC() {
    // Update amount of specified RIC's so far for use in JSON parsing of the
    // form later.
    RIC_count++;

    // Create the input heading "RIC" which will appear above the input text field.
    var inputHeading = $('<p></p>', {
        text: 'RIC'
    })
    inputHeading.insertBefore($('#set-add-new-ric'));

    // Create the text input
    var selectField = $('<select>', {
        name: "RIC_" + RIC_count,
        class: "form-control ric-input",
        form: "set-study-form",
        placeholder: "RIC Stock ID"
    })
    selectField.insertBefore($('#set-add-new-ric'));

    for (var key in ricDropdown) {
        if (!ricDropdown.hasOwnProperty(key)) continue;
        selectField.append($("<option>").attr('value',ricDropdown[key]).text(ricDropdown[key]));
    };

    // Create the button to delete the "RIC" attribute
    var deleteButton = $('<button></button>', {
        text: "x",
        class: "btn btn-danger btn-sm ric-delete",
        id: "delete_RIC_" + RIC_count,
        click: function () {
            deleteRIC(inputHeading, selectField, deleteButton);
        }
    })
    deleteButton.insertBefore($('#set-add-new-ric'));
}

// Function that gets run once to intiliase page
$(document).ready(function () {
    // Hide add new ric button and add another parameter and it's heading
    // until the dropdown is populated.
    $('#add-new-ric').hide();
    $('#extra-params-heading').hide();
    $('#add-new-param').hide();

    // Add listeners for particular events.
    $('#stock-data-upload-form').submit(function(e) {
        e.preventDefault();
        $('#stock-loader').show();
        $.ajax({
            url:'/upload',
            type:'POST',
            data: new FormData($('#stock-data-upload-form')[0]),
            processData: false,
            contentType: false,
            success: function(output) {
                $('#file1Hash').val(output);
                $('#stock-loader').hide();
            },
            error: function(output){
                $('#stock-loader').hide();
            }
        });
    });

    // Hide the refresh button until the user clicks on the file 2 hash form element
    // to fill out the hash 2 manually.
    $('#refresh-dropdowns-button').hide();
    $('#file2Hash').click(function() {
        $('#refresh-dropdowns-button').show();
    });

    $('#event-data-upload-form').submit(function(e) {
        e.preventDefault();
        $('#event-loader').show();
        $.ajax({
            url:'/upload',
            type:'POST',
            data: new FormData($('#event-data-upload-form')[0]),
            processData: false,
            contentType: false,
            success: function(output) {
                $('#file2Hash').val(output);
                $.ajax({
                    url:'/rics',
                    type:'GET',
                    data: {"file": output},
                    success: function (rics){
                        generateRICDropdown(rics);
                    }
                })
                $.ajax({
                    url:'/params',
                    type:'GET',
                    data: {"file": output},
                    success: function (params){
                        console.log(params);
                        generateParamDropdown(params);
                    }
                })
                $('#event-loader').hide();
            }
        });
    });

    form.submit(function(e){
        $('#study-loader').show();
        e.preventDefault();
        var upper = form[0]['upperWindow'].value;
        var lower = form[0]['lowerWindow'].value;
        if (upper < 0 || lower > 0){
            alert('Your input is invalid. Upper window should be a positive value and lower window should be a negative value.');
        } else if (!upper || !lower) {
            alert('Please enter a value for the upper window and the lower window.');
        } else {
            $.ajax({
                'Content-Type': 'json',
                url:'/new-study',
                type:'POST',
                dataType: 'json',
                data: form.serializeArray(),
                success: function(output) {
                    $('#study-loader').hide();
                    $("#study-wrapper").toggleClass("toggled");
                    if (output.hasOwnProperty('errorMessage')){
                        displayError(output);
                    } else {
                        displayGraph(output);
                    }
                },

            });
        }
    });

    $('#set-study-form').submit(function(e){
        $('#set-study-loader').show();
        e.preventDefault();
        var upper = $('#set-study-form')[0]['upperWindow'].value;
        var lower = $('#set-study-form')[0]['lowerWindow'].value;
        if (upper < 0 || lower > 0){
            alert('Your input is invalid. Upper window should be a positive value and lower window should be a negative value.');
        } else if (!upper || !lower) {
            alert('Please enter a value for the upper window and the lower window.');
        } else {
            $.ajax({
                'Content-Type': 'json',
                url:'/set-new-study',
                type:'POST',
                dataType: 'json',
                data: $('#set-study-form').serializeArray(),
                success: function(output) {
                    $('#set-study-loader').hide();
                    $("#set-wrapper").toggleClass("toggled");
                    if (output.hasOwnProperty('errorMessage')){
                        displayError(output);
                    } else {
                        displayGraph(output);
                    }
                },

            });
        }
    });

    // Set up menu-toggle capabilities
    $("#study-toggle").click(function(e) {
        e.preventDefault();
        if (!$('#set-wrapper').hasClass('toggled'))
        {
            $("#set-wrapper").toggleClass("toggled");
        }        
        $("#study-wrapper").toggleClass("toggled");
    });

    $("#set-toggle").click(function(e) {
        e.preventDefault();
        if (!$('#study-wrapper').hasClass('toggled'))
        {
            $("#study-wrapper").toggleClass("toggled");
        } 
        $("#set-wrapper").toggleClass("toggled");
    });

    $( '.inputfile' ).each( function()
    {
        var $input   = $( this ),
            $label   = $input.next( 'label' ),
            labelVal = $label.html();

        $input.on( 'change', function( e )
        {
            var fileName = '';

            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else if( e.target.value )
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                $label.find( 'span' ).html( fileName );
            else
                $label.html( labelVal );
        });

        // Firefox bug fix
        $input
        .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
        .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    });

    $('#set-data-select').change(function(e) {
        paramDropdown = setEvents;
        ricDropdown = setRICS[$('#set-data-select').val()];
        //addNewParam();
        // addNewRic();
    });
});
